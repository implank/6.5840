package mr

import (
	"encoding/json"
	"fmt"
	"hash/fnv"
	"io/ioutil"
	"log"
	"net/rpc"
	"os"
	"os/exec"
	"sort"
	"strings"
)

type ByKey []KeyValue

func (a ByKey) Len() int           { return len(a) }
func (a ByKey) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByKey) Less(i, j int) bool { return a[i].Key < a[j].Key }

// Map functions return a slice of KeyValue.
type KeyValue struct {
	Key   string
	Value string
}

// use ihash(key) % NReduce to choose the reduce
// task number for each KeyValue emitted by Map.
func ihash(key string) int {
	h := fnv.New32a()
	h.Write([]byte(key))
	return int(h.Sum32() & 0x7fffffff)
}

// main/mrworker.go calls this function.
func Worker(mapf func(string, string) []KeyValue,
	reducef func(string, []string) string) {
	// Your worker implementation here.
	// uncomment to send the Example RPC to the coordinator.
	for {
		var reply ReqTaskReply
		call("Coordinator.GetTask", &ReqTaskArgs{}, &reply)
		fmt.Println("get task:", reply)
		if reply.TaskType == "none" {
			break
		} else if reply.TaskType == "map" {
			file, err := os.Open(reply.Filename)
			if err != nil {
				log.Fatalf("cannot open %v", reply.Filename)
			}
			content, err := ioutil.ReadAll(file)
			if err != nil {
				log.Fatalf("cannot read %v", reply.Filename)
			}
			file.Close()
			kva := mapf(reply.Filename, string(content))
			// output to intermediate files
			files := make([]*os.File, reply.NReduce)
			encoders := make([]*json.Encoder, reply.NReduce)
			for i := 0; i < reply.NReduce; i++ {
				file, err := ioutil.TempFile("", "")
				if err != nil {
					log.Fatal("create tmpFile error")
				}
				defer file.Close()
				files[i] = file
				encoders[i] = json.NewEncoder(file)
			}
			for _, kv := range kva {
				idx := ihash(kv.Key) % reply.NReduce
				encoders[idx].Encode(&kv)
			}
			for i := 0; i < reply.NReduce; i++ {
				os.Rename(files[i].Name(), fmt.Sprintf("mr-%v-%v", reply.TaskID, i))
			}
			call("Coordinator.InformTaskDone", &InformTaskDoneArgs{
				TaskType: "map",
				TaskID:   reply.TaskID,
				Filename: reply.Filename,
			}, &InformTaskDoneReply{})
		} else if reply.TaskType == "reduce" {
			kva := []KeyValue{}

			lsCmd := fmt.Sprintf("ls mr-*-%v", reply.Filename)
			lsOutput, err := exec.Command("sh", "-c", lsCmd).Output()
			if err != nil {
				log.Fatalf("cannot list files")
			}
			files := strings.Split(string(lsOutput), "\n")
			files = files[:len(files)-1]
			outputFile, _ := ioutil.TempFile("", "")
			for _, filename := range files {
				file, err := os.Open(filename)
				if err != nil {
					log.Fatalf("cannot open %v", filename)
				}
				dec := json.NewDecoder(file)
				for {
					var kv KeyValue
					if err := dec.Decode(&kv); err != nil {
						break
					}
					kva = append(kva, kv)
				}
				file.Close()
			}
			sort.Sort(ByKey(kva))
			i := 0
			for i < len(kva) {
				j := i + 1
				for j < len(kva) && kva[j].Key == kva[i].Key {
					j++
				}
				values := []string{}
				for k := i; k < j; k++ {
					values = append(values, kva[k].Value)
				}
				output := reducef(kva[i].Key, values)
				fmt.Fprintf(outputFile, "%v %v\n", kva[i].Key, output)
				i = j
			}
			os.Rename(outputFile.Name(), fmt.Sprintf("mr-out-%v", reply.Filename))
			call("Coordinator.InformTaskDone", &InformTaskDoneArgs{
				TaskType: "reduce",
				TaskID:   reply.TaskID,
				Filename: reply.Filename,
			}, &InformTaskDoneReply{})
		} else {
			panic("unknown task type")
		}
	}
}

// example function to show how to make an RPC call to the coordinator.
//
// the RPC argument and reply types are defined in rpc.go.
func CallExample() {

	// declare an argument structure.
	args := ExampleArgs{}

	// fill in the argument(s).
	args.X = 99

	// declare a reply structure.
	reply := ExampleReply{}

	// send the RPC request, wait for the reply.
	// the "Coordinator.Example" tells the
	// receiving server that we'd like to call
	// the Example() method of struct Coordinator.
	ok := call("Coordinator.Example", &args, &reply)
	if ok {
		// reply.Y should be 100.
		fmt.Printf("reply.Y %v\n", reply.Y)
	} else {
		fmt.Printf("call failed!\n")
	}
}

// send an RPC request to the coordinator, wait for the response.
// usually returns true.
// returns false if something goes wrong.
func call(rpcname string, args interface{}, reply interface{}) bool {
	// c, err := rpc.DialHTTP("tcp", "127.0.0.1"+":1234")
	sockname := coordinatorSock()
	c, err := rpc.DialHTTP("unix", sockname)
	if err != nil {
		log.Fatal("dialing:", err)
	}
	defer c.Close()

	err = c.Call(rpcname, args, reply)
	if err == nil {
		return true
	}

	fmt.Println(err)
	return false
}
