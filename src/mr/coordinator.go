package mr

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"net/rpc"
	"os"
	"sync"
	"sync/atomic"
	"time"
)

type Coordinator struct {
	// Your definitions here.
	Tasks      chan string
	TaskStatus map[string]int
	Status     string `enums:"map,reduce,none"`
	TaskIDGen  atomic.Int32

	NReduce int
	m       sync.Mutex
}

// Your code here -- RPC handlers for the worker to call.

// an example RPC handler.
//
// the RPC argument and reply types are defined in rpc.go.
func (c *Coordinator) Example(args *ExampleArgs, reply *ExampleReply) error {
	reply.Y = args.X + 1
	return nil
}

// start a thread that listens for RPCs from worker.go
func (c *Coordinator) server() {
	rpc.Register(c)
	rpc.HandleHTTP()
	//l, e := net.Listen("tcp", ":1234")
	sockname := coordinatorSock()
	os.Remove(sockname)
	l, e := net.Listen("unix", sockname)
	if e != nil {
		log.Fatal("listen error:", e)
	}
	go http.Serve(l, nil)
}

func (c *Coordinator) Wait(filename string, timeout time.Duration) {
	time.Sleep(timeout)
	c.m.Lock()
	defer c.m.Unlock()
	if status, ok := c.TaskStatus[filename]; ok {
		if status == -1 {
			c.Tasks <- filename
		}
	}
}

func (c *Coordinator) GetTask(args *ReqTaskArgs, reply *ReqTaskReply) error {
	task, ok := <-c.Tasks
	if !ok {
		reply.TaskType = "none"
		return nil
	}
	reply.Filename = task
	reply.TaskType = c.Status
	c.TaskIDGen.Add(1)
	reply.TaskID = int(c.TaskIDGen.Load())
	reply.NReduce = c.NReduce
	go c.Wait(task, 10*time.Second)
	return nil
}

func (c *Coordinator) InformTaskDone(args *InformTaskDoneArgs, reply *InformTaskDoneReply) error {
	c.m.Lock()
	defer c.m.Unlock()
	c.TaskStatus[args.Filename] = args.TaskID
	if c.Status == "map" {
		for _, status := range c.TaskStatus {
			if status == -1 {
				return nil
			}
		}
		c.Status = "reduce"
		c.TaskStatus = make(map[string]int)
		for i := 0; i < c.NReduce; i++ {
			c.Tasks <- fmt.Sprintf("%v", i)
			c.TaskStatus[fmt.Sprintf("%v", i)] = -1
		}
		log.Println("All map tasks done")
	} else if c.Status == "reduce" {
		for _, status := range c.TaskStatus {
			if status == -1 {
				return nil
			}
		}
		close(c.Tasks)
		c.Status = "none"
		log.Println("All reduce tasks done")
	}
	return nil
}

// main/mrcoordinator.go calls Done() periodically to find out
// if the entire job has finished.
func (c *Coordinator) Done() bool {
	ret := false

	if c.Status == "none" {
		ret = true
	}

	return ret
}

// create a Coordinator.
// main/mrcoordinator.go calls this function.
// nReduce is the number of reduce tasks to use.
func MakeCoordinator(files []string, nReduce int) *Coordinator {
	c := Coordinator{
		Tasks:      make(chan string, len(files)+nReduce),
		TaskStatus: make(map[string]int),
		TaskIDGen:  atomic.Int32{},
		Status:     "map",

		m:       sync.Mutex{},
		NReduce: nReduce,
	}
	for _, file := range files {
		c.Tasks <- file
		c.TaskStatus[file] = -1
	}

	fmt.Println(files)

	c.server()
	return &c
}
